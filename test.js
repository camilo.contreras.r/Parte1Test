'use strict';

// This class is used for logins
class Login {
  constructor(hash) {
    this.sessions = [];
    this.users = [];
    this.passwords = [];
    // se inicializa variable map con el resultado del registro de cada usuario seteado
    this.map=Object.keys(hash).map(user =>  this.registerUser(user, hash[user]));
  }
  // se busca el usuario utilizando la función idx y se remueve el item de la sesión utilizando splice
  logout(user) {
    let index = this.idx(user, this.sessions);
    return index !== -1 && this.sessions.splice(index,1);
  }
  // Método devuelve true o false si encuentra coincidencia del elemento a buscar
  userExists(user) {
    return this.idx(user, this.users) !== -1 ? true : false;
  }

  // Se valida que los parámetros no vengan vacíos y se agregan los elementos utilizando push
  registerUser(user ='', password = '') {
    if (!this.userExists(user) && user !== '' && password !== ''){
      this.users.push(user);
      this.passwords.push(password);
      return true;
    } else {
      return false;
    }
  }
  // Se utiliza splice para remover un elemento del arreglo
  removeUser(user) {
    if (this.userExists(user)) {
      let index = this.idx(user, this.users);
      return index !== -1 && this.users.splice(index,1) && this.passwords.splice(index,1)
    } else {
      return false;
    }
  }
  //  Se identifica si el usuario existe y se compara con el elemento
  checkPassword(user, password) {
    let index = this.idx(user, this.users);
    return this.passwords[index] === password;
  }
  //  Si oldPassword es distinto de newPassword se realiza el remplazo del dato
  updatePassword(user, oldPassword, newPassword) {
    let index = this.idx(user, this.users);
    if (index !== -1){
      if (this.passwords[index] === oldPassword) {
        this.passwords[index] = newPassword;
        return true;
      }
    } else {
      return false;
    }
  }
  //  Se valida que el usuario no tenga una sesión activa
  login(user, password) {
    if (this.idx(user, this.sessions) !== -1) {
      return false;
    } else {
      return this.checkPassword(user, password) && this.sessions.push(user);
    }
  }
  // indexOf para obtener el index del elemento a buscar
  idx(element, array) {
    return array.indexOf(element)
  }
}

let registeredUsers = {
  user1: 'pass1',
  user2: 'pass2',
  user3: 'pass3'
};

let login = new Login(registeredUsers);

login.registerUser('user4', 'pass4');
login.login('user4', 'pass4');
login.updatePassword('user3', 'pass3', 'pass5');
login.login('user3', 'pass5');
login.logout('user4');
login.logout('user3');